package auxiliar;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RangoResolucion{
	public enum Resolucion {DIA,MINU_20,MAX,BAJA,MIN};
	
	
	static SimpleDateFormat ymd =  new SimpleDateFormat("yyyy-MM-dd");
	public Resolucion resolucion;
	public Calendar inicio;
	public Calendar fin;
	public RangoResolucion(Resolucion resolucion, Calendar inicio, Calendar fin) {
		super();
		this.resolucion = resolucion;
		this.inicio = inicio;
		this.fin = fin;
	}
	
	@Override 
	public String toString() {
		// TODO Auto-generated method stub
		return resolucion.toString() + "@[" + ymd.format(inicio.getTime()) + "," + ymd.format(fin.getTime()) + "]";
	}
	
}