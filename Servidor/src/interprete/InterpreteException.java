package interprete;

public class InterpreteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InterpreteException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InterpreteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InterpreteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InterpreteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
