package ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;

import auxiliar.RangoResolucion;
import auxiliar.RangoResolucion.Resolucion;


public class TestResolucion {

	
	/*Tabla de Resoluciones:
	 * (,min_diario) => MIN (cada 2 años)
	 * [min_diario, fin_solo_historico] => MAX (optimizado)
	 * [min_diario, min_20) => DIA
	 * [min_20, min_tiempo_real) => 20 minutos
	 * [min_tiempo_Real, ) => MAX
	 * 
	 * min_20 = -7d
	 * min_diario = min_20 - 2y
	 * fin_solo_historico = 2015-10-01
	 * min_tiempo_real = 1d habil
	 * 
	 */
	
	/**
	 * Comprueba que la division en rangos es una particion del rango de entrada
	 */
	@Test
	public void particion1(){
		EjbCalculoSaldo ejb = new EjbCalculoSaldo();
		GregorianCalendar inicio = new GregorianCalendar(2009,Calendar.JANUARY,1);
		GregorianCalendar fin = new GregorianCalendar(2015,Calendar.DECEMBER,28);
		List<RangoResolucion> rangos = ejb.obtenerResoluciones(inicio, fin);
		RangoResolucion p = rangos.get(0);
		for(int i = 1; i < rangos.size(); i++){
			RangoResolucion r = rangos.get(i);
			assertTrue("inicio rangos no creciente " + p.toString() + " - "  + r.toString(), r.inicio.after(p.inicio));
			assertTrue("fin rangos no creciente " + p.toString() + " - "  + r.toString(), r.fin.after(p.fin));
			assertFalse("rangos superpuestos " + p.toString() + " - "  + r.toString(), r.inicio.before(p.fin));
			assertFalse("rangos no continuos " + p.toString() + " - "  + r.toString(), r.inicio.after(p.fin));
			p = r;
		}
	}
	
	@Test
	public void divisiones1(){
		EjbCalculoSaldo ejb = new EjbCalculoSaldo();
		GregorianCalendar inicio = new GregorianCalendar(2009,Calendar.JANUARY,1);
		GregorianCalendar fin = new GregorianCalendar(2015,Calendar.DECEMBER,1);
		List<RangoResolucion> rangos = ejb.obtenerResoluciones(inicio, fin);
		RangoResolucion r_min0 = new RangoResolucion(Resolucion.MIN, inicio, new GregorianCalendar(2011,Calendar.JANUARY,1));
		RangoResolucion r_min1 = new RangoResolucion(Resolucion.MIN, new GregorianCalendar(2011,Calendar.JANUARY,1), new GregorianCalendar(2013,Calendar.JANUARY,1));
		RangoResolucion r_min2 = new RangoResolucion(Resolucion.MIN, new GregorianCalendar(2013,Calendar.JANUARY,1), new GregorianCalendar(2013,Calendar.NOVEMBER,24));
		RangoResolucion r_max3 = new RangoResolucion(Resolucion.MAX, new GregorianCalendar(2013,Calendar.NOVEMBER,24), new GregorianCalendar(2015,Calendar.OCTOBER,1));
		RangoResolucion r_dia4 = new RangoResolucion(Resolucion.DIA, new GregorianCalendar(2015,Calendar.OCTOBER,1), new GregorianCalendar(2015,Calendar.NOVEMBER,24));
		RangoResolucion r_m20_5 = new RangoResolucion(Resolucion.MINU_20,  new GregorianCalendar(2015,Calendar.NOVEMBER,24),new GregorianCalendar(2015,Calendar.NOVEMBER,30));
		RangoResolucion r_max_6 = new RangoResolucion(Resolucion.MAX,  new GregorianCalendar(2015,Calendar.NOVEMBER,30),new GregorianCalendar(2015,Calendar.DECEMBER,1));
		System.out.println("Rangos:");
		for(RangoResolucion r : rangos){
			System.out.println(r.toString());
		}
		
		assertEquals("nro rangos",7, rangos.size());
		assertEquals("r6",r_max_6.toString(), rangos.get(6).toString());
		assertEquals("r5",r_m20_5.toString(), rangos.get(5).toString());
		assertEquals("r4",r_dia4.toString(), rangos.get(4).toString());
		assertEquals("r3",r_max3.toString(), rangos.get(3).toString());
		assertEquals("r2",r_min2.toString(), rangos.get(2).toString());
		assertEquals("r1",r_min1.toString(), rangos.get(1).toString());
		assertEquals("r0",r_min0.toString(), rangos.get(0).toString());

		
	}
	
	@Test
	public void divisiones2(){
		EjbCalculoSaldo ejb = new EjbCalculoSaldo();
		GregorianCalendar inicio = new GregorianCalendar(2009,Calendar.JANUARY,1);
		GregorianCalendar fin = new GregorianCalendar(2015,Calendar.NOVEMBER,29);
		List<RangoResolucion> rangos = ejb.obtenerResoluciones(inicio, fin);
		RangoResolucion r_min0 = new RangoResolucion(Resolucion.MIN, inicio, new GregorianCalendar(2011,Calendar.JANUARY,1));
		RangoResolucion r_min1 = new RangoResolucion(Resolucion.MIN, new GregorianCalendar(2011,Calendar.JANUARY,1), new GregorianCalendar(2013,Calendar.JANUARY,1));
		RangoResolucion r_min2 = new RangoResolucion(Resolucion.MIN, new GregorianCalendar(2013,Calendar.JANUARY,1), new GregorianCalendar(2013,Calendar.NOVEMBER,22));
		RangoResolucion r_max3 = new RangoResolucion(Resolucion.MAX, new GregorianCalendar(2013,Calendar.NOVEMBER,22), new GregorianCalendar(2015,Calendar.OCTOBER,1));
		RangoResolucion r_dia4 = new RangoResolucion(Resolucion.DIA, new GregorianCalendar(2015,Calendar.OCTOBER,1), new GregorianCalendar(2015,Calendar.NOVEMBER,22));
		RangoResolucion r_m20_5 = new RangoResolucion(Resolucion.MINU_20,  new GregorianCalendar(2015,Calendar.NOVEMBER,22),new GregorianCalendar(2015,Calendar.NOVEMBER,27));
		RangoResolucion r_max_6 = new RangoResolucion(Resolucion.MAX,  new GregorianCalendar(2015,Calendar.NOVEMBER,27),new GregorianCalendar(2015,Calendar.NOVEMBER,29));
		System.out.println("Rangos:");
		for(RangoResolucion r : rangos){
			System.out.println(r.toString());
		}
		
		assertEquals("nro rangos",7, rangos.size());
		assertEquals("r6",r_max_6.toString(), rangos.get(6).toString());
		assertEquals("r5",r_m20_5.toString(), rangos.get(5).toString());
		assertEquals("r4",r_dia4.toString(), rangos.get(4).toString());
		assertEquals("r3",r_max3.toString(), rangos.get(3).toString());
		assertEquals("r2",r_min2.toString(), rangos.get(2).toString());
		assertEquals("r1",r_min1.toString(), rangos.get(1).toString());
		assertEquals("r0",r_min0.toString(), rangos.get(0).toString());

		
	}

}
